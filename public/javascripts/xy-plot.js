//stored as global so future updates on the render don't have to refetch the data.
var data;
function plot() {
    get_data();
}

function get_data(callback) {
    var req = {};
    req.companies = {};
    //get a list of selected companies
    var companies = $("#company option:selected")
    //loop through the companies and check to see if all is selected
    for (var i = 0; i < companies.length; i++) {
        if (companies[i].value == "all") {
            companies = $("#company option:visible");
            break;
        }
    }
    req.companies = [];
    for (var i = 0; i < companies.length; i++) {
        if (companies[i].value != 'all') {
            req.companies.push(companies[i].value);
        }
    }
    req.metrics = {
        x: $("#x option:selected")[0].value,
        y: $("#y option:selected")[0].value,
        size: $("#size option:selected")[0].value
    };


    //make a request to the database to get the companies data in the form of [companise] metric-x, metric-y, metric-size, cluster-type
    $.ajax({
        url: '/get_data',
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8', // This is the money shot
        data: JSON.stringify(req),
        type: 'POST',
        complete: function (res) {
            json = JSON.parse(res.responseText);
            //json in the form of comapanies[ {name, data { metricX, metricY, metricSize, date}}...]}]

            //Clusterise the data
            cluster_data(json);


            //mock cluster to test that it works.
            //var cluster1 = {"cluster": "cluster1", companies: []};
            //var cluster2 = {"cluster": "cluster2", companies: []};
            //
            //for (var i = 0; i < json.length; i++) {
            //    if (i % 2 == 0) {
            //        cluster1.companies.push(json[i]);
            //    }
            //    else {
            //        cluster2.companies.push(json[i]);
            //    }
            //}
            //
            //data = [cluster1, cluster2];
        }
    });


    //store as global data.
    //data then needs to be transformed to cluster, x, y, size, based on the date slider
    //data then needs to be plotted
}

function cluster_data(raw) {
    //detect the method for clustering
    var cluster_method = $("#cluster option:selected")[0].value;
    if(cluster_method == 'none') {
        data = [{cluster: "none", companies: raw}];
        render();
    }
    else if (cluster_method == 'km'){
        $.ajax({
            url: 'https://5t17j3c4ub.execute-api.ap-southeast-2.amazonaws.com/prod/kmeans',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(raw),
            type: 'POST',
            complete: function (res) {
                var resdata;
                if(res.responseText){
                    resdata = JSON.parse(res.responseText);
                }
                //json in the form of comapanies[ {name, data { metricX, metricY, metricSize, date}}...]}]
                console.log(resdata);
                data = resdata;
                render();
            }
        });
    }
    else if(cluster_method == 'kmpp'){
        $.ajax({
            url: 'https://5t17j3c4ub.execute-api.ap-southeast-2.amazonaws.com/prod/kmeanspp',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(raw),
            type: 'POST',
            complete: function (res) {
                var resdata;
                if(res.responseText){
                    resdata = JSON.parse(res.responseText);
                }
                //json in the form of comapanies[ {name, data { metricX, metricY, metricSize, date}}...]}]
                console.log(resdata);
                data = resdata;
                render();
            }
        });
    }
    else if(cluster_method == 'random'){
        $.ajax({
            url: 'https://5t17j3c4ub.execute-api.ap-southeast-2.amazonaws.com/prod/random',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(raw),
            type: 'POST',
            complete: function (res) {
                var resdata;
                if(res.responseText){
                    resdata = JSON.parse(res.responseText);
                }
                //json in the form of comapanies[ {name, data { metricX, metricY, metricSize, date}}...]}]
                console.log(resdata);
                data = resdata;
                render();
            }
        });
    }
}

function render() {

    //Get the value on the range slider
    var cdate = $('#date')[0].value;

    // to help with calibrate the size ref
    //find the largest x
    var xLarge = 0e0;
    var xSmall = 0e0;
    //find the largest y
    var yLarge = 0e0;
    var ySmall = 0e0;
    //find the largest size
    var sizeLarge = 0e0;

    //Transform the data into an array of clusters
    render_data = [];

    //loop through each cluster and make the cluster framework
    if (data) {

        for (var i = 0; i < data.length; i++) {
            var cluster = {
                mode: 'markers',
                name: data[i].cluster,
                x: [],
                y: [],
                text: [],
                marker: {
                    sizemode: 'area',
                    size: [],
                    sizeref: 1
                }
            };

            //loop through each company and populate the cluster.
            for (var j = 0; j < data[i].companies.length; j++) {
                //loop through each company's data to check to see if they have data in this date range
                for (var k = 0; k < data[i].companies[j].data.length; k++) {
                    if (data[i].companies[j].data[k].date == cdate) {
                        var x = Number(data[i].companies[j].data[k].metricX);
                        var y = Number(data[i].companies[j].data[k].metricY);
                        var size = Number(data[i].companies[j].data[k].metricSize);

                        cluster.x.push(x);
                        if (x > xLarge) {
                            xLarge = x;
                        }
                        if (x < xSmall) {
                            xSmall = x;
                        }
                        cluster.y.push(y);
                        if (y > yLarge) {
                            yLarge = y;
                        }
                        if (y < ySmall) {
                            ySmall = y;
                        }

                        cluster.marker.size.push(size);
                        if (size > sizeLarge) {
                            sizeLarge = size;
                        }

                        cluster.text.push(data[i].companies[j].code);
                        break;
                    }
                }
            }

            //find the width of the div
            var width = $(document).width();
            //set the height of the div
            var height = 800;

            //Add sizeRef to the cluster obejct


            //populate the renderdata object
            render_data.push(cluster);
        }

        //make the largest size the size of 5000
        var sizeRef = sizeLarge / 10000;

        for(i = 0; i < render_data.length; i++){
            render_data[i].marker.sizeref = sizeRef;
        }


        //Ensure there is enough room to show the full blob
        ySmall = ySmall - (0.1 * ySmall);
        ylarge = yLarge + (0.1 * yLarge);
        if (ySmall == 0) {
            ySmall = -0.1 * yLarge;
        }

        xSmall = xSmall - (0.1 * xSmall);
        xLarge = xLarge + (0.1 * xLarge);
        if (xSmall == 0) {
            xSmall = -0.1 * xLarge;
        }

        var layout = {

            margin: {t: 0},
            xaxis: {
                range: [xSmall, xLarge],
                title: $("#x option:selected")[0].text
            },
            yaxis: {
                range: [ySmall, yLarge],
                title: $("#y option:selected")[0].text
            }
        };

        GRAPH = document.getElementById('graph');
        Plotly.newPlot(GRAPH, render_data, layout);
    }
    else {
        alert("No Data to plot.");
    }


}