function filter_entry(){
    var select = $("#sector option:selected");
    //check to see if the sector is selected
    var companies = $("#company option");
    for(var i = 0; i < companies.length; i++) {
        if (companies[i].value == "all") {
            continue;
        }
        var selected = false;
        for (var j = 0; j < select.length; j++) {
            if (select[j].value == "all") {
                selected = true;
                break;
            }
            if ($(companies[i]).data("sector") == select[j].value) {
                selected = true;
            }
        }

        //if all three are selected, set display to inline
        if (selected) {
            $(companies[i]).show();
        }
        else {
            $(companies[i]).hide();
        }
    }
    hide_fama();
    filter_fama();
}

function hide_fama(){
    var fama_select = $( "#fama option" );
    var companies = $("#company option");
    for(var i = 0; i < fama_select.length; i++) {
        var fama_selected = false;
        if (fama_select[i].value == "all") {
            continue;
        }

        for(var j = 0; j < companies.length; j++){
            if(companies[j].value == "all"){
                continue;
            }
            if(companies[j].style.display == 'none'){
                continue;
            }
            if($(companies[j]).data("fama") == fama_select[i].value){
                fama_selected = true;
            }
        }
        if(fama_selected){
            $(fama_select[i]).show();
        }
        else{
            $(fama_select[i]).hide();
        }
    }
}

function filter_fama(){
    var select = $("#fama option:selected");
    //check to see if the sector is selected
    var companies = $("#company option");
    for(var i = 0; i < companies.length; i++) {
        if (companies[i].value == "all") {
            continue;
        }
        if(companies[i].style.display == "none"){
            continue;
        }
        var selected = false;
        for (var j = 0; j < select.length; j++) {
            if (select[j].value == "all") {
                selected = true;
                break;
            }
            if ($(companies[i]).data("fama") == select[j].value) {
                selected = true;
            }
        }
        //if all three are selected, set display to inline
        if (selected) {
            $(companies[i]).show();
        }
        else {
            $(companies[i]).hide();
        }
    }
    hide_industry();
    filter_industry();
}

function hide_industry(){
    var industry_select = $( "#industry option" );
    var companies = $("#company option");
    for(var i = 0; i < industry_select.length; i++) {
        var industry_selected = false;
        if (industry_select[i].value == "all") {
            continue;
        }

        for(var j = 0; j < companies.length; j++){
            if(companies[j].value == "all"){
                continue;
            }
            if(companies[j].style.display == 'none'){
                continue;
            }
            if($(companies[j]).data("industry") == industry_select[i].value){
                industry_selected = true;
            }
        }
        if(industry_selected){
            $(industry_select[i]).show();
        }
        else{
            $(industry_select[i]).hide();
        }
    }
}

function filter_industry(){
    var select = $("#industry option:selected");
    //check to see if the sector is selected
    var companies = $("#company option");
    for(var i = 0; i < companies.length; i++) {
        if (companies[i].value == "all") {
            continue;
        }
        if(companies[i].style.display == "none"){
            continue;
        }
        var selected = false;
        for (var j = 0; j < select.length; j++) {
            if (select[j].value == "all") {
                selected = true;
                break;
            }
            if ($(companies[i]).data("industry") == select[j].value) {
                selected = true;
            }
        }
        //if all three are selected, set display to inline
        if (selected) {
            $(companies[i]).show();
        }
        else {
            $(companies[i]).hide();
        }
    }
}