var fs = require('fs');
var parse = require('csv-parse');
var moment = require('moment');
//Set up postgres db config
var pg = require('pg');
var connectionString = "postgres://sa:me8aEqgqjx@equaldb.cz5umxgcsyeu.ap-southeast-2.rds.amazonaws.com:5432/equaldb";
var client = new pg.Client(connectionString);

var path = "../data/fundamentals.csv";

////Include aws sdk
//var AWS = require('aws-sdk');
////Configure sdk to the correct account
//AWS.config.update({
//    accessKeyId: "AKIAJ6Q6GDWJCICCYWBQ",
//    secretAccessKey: "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",
//    region: "ap-southeast-2",
//    endpoint: "dynamodb.ap-southeast-2.amazonaws.com"
//});
////Create dynamodb controller
//var dynamodb = new AWS.DynamoDB();
//var docClient = new AWS.DynamoDB.DocumentClient();
//var tablename = "fundamental";


var db_loader = {
    create_db: function(callback) {
        client.connect();
        //Create the database table

        var line1 = client.query('DROP TABLE data');
        line1.on('end', function(){
            var line2 = client.query('CREATE TABLE data(id SERIAL PRIMARY KEY, code CHAR(5) not null, metric VARCHAR(20), date DATE, value NUMERIC);');
            line2.on('end', db_loader.load);
        });

        ////OLD AWS CODE
        //
        ////First call to describe table to check if the table exists.
        //var params = {
        //    TableName: tablename
        //};
        //dynamodb.describeTable(params, function(err, data) {
        //    //if there is an error
        //    if (err){
        //        //if the error is there is no table
        //        if(err.code == "ResourceNotFoundException"){
        //            console.log("no old table found, creating new table....");
        //            var params = {
        //                TableName : tablename,
        //                KeySchema: [
        //                    { AttributeName: "code", KeyType: "HASH"},  //Partition key
        //                ],
        //                AttributeDefinitions: [
        //                    { AttributeName: "code", AttributeType: "S" },
        //                ],
        //                ProvisionedThroughput: {
        //                    ReadCapacityUnits: 5,
        //                    WriteCapacityUnits: 5
        //                }
        //            };
        //            dynamodb.createTable(params, function(err, data) {
        //                if (err) {
        //                    console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
        //                } else {
        //                    console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
        //                    //if the table doesn't exist, wait until the callback for create table has finished before calling createTable function.
        //                    db_loader.load(callback);
        //                }
        //            });
        //
        //        }
        //
        //    }
        //    //if the table already exists, call the load function.
        //    else{
        //        db_loader.load(callback);
        //    }
        //});
        //

    },
    load: function(callback){
        console.log("Reading file from disk...");
        var count = 0;
        var parser = parse({delimiter: ','}, function (err, data) {
            console.log("Parsing csv...");

            for(var i = 0; i < data.length; i++){
                //Parse the date as a date
                var date = moment(data[i][2], "DD/MM/YYYY");
                //Save it as ISO8601 standard date
                data[i][2] = date.format("YYYY-MM-DD");
                var query = client.query("INSERT INTO data(code, metric, date, value) VALUES( $1, $2, $3, $4);", data[i]);
                query.on("end", function(){console.log("Inserted Company ", ++count)});
            }



            ////OLD AWS CODE
            ////start from the first company
            //var current_company = {
            //    code:data[0][0],
            //    data:[]
            //};
            //for(var i = 0; i < data.length; i++){
            //    //for each row of data, check to see if it's part of the current company
            //    if(current_company.code && current_company.code == data[i][0]){
            //            current_company.data.push({
            //                    metric:data[i][1],
            //                    date:data[i][2],
            //                    value:data[i][3]
            //                });
            //    }
            //    //otherwise save this company and move on to the next company
            //    else{
            //        //check to make sure the company has a code
            //        if(current_company.code){
            //            //If the company has as code, save the data.
            //            var params = {
            //                TableName:tablename,
            //                Item: current_company
            //            };
            //            docClient.put(params, function(err){
            //                count++;
            //                if(err){
            //                    console.log("Put Failed:", err);
            //                }
            //                else {
            //                    console.log("added company ", count, " Successfully.");
            //                }
            //            });
            //        }
            //        current_company = {
            //            code:data[i][0],
            //            data:[]
            //        };
            //    }
            //}
        });
        //Execute everything
        fs.createReadStream(path).pipe(parser);
    }
};

module.exports = db_loader;