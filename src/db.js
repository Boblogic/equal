var moment = require('moment');
//Set up postgres db config
var pg = require('pg');
var credentails = require("../credentials/credentials.js");
var connectionString = credentails.db;

var db = {
    query: function(text, values, callback) {
        pg.connect(connectionString, function(err, client, done){
            if(err){
                console.log(err);
            }
            client.query(text, values,function(err, result){
                if(done){
                    done();
                }
                if(callback){
                    callback(err,result);
                }
            })
        });
    }
};

module.exports = db;