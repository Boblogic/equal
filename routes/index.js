var express = require('express');
var moment = require('moment');
var dbloader = require('../src/dbloader.js');
var router = express.Router();
var db = require('../src/db.js');
var http = require('http');


/* GET home page. */
router.get('/', function(req, res, next) {
  var render_info = {
    statements:[],
    data:[]
  };
  //Get the statements available for build the financial statement objects. Each financial statement object has a name and a list of metrics.
  db.query("SELECT * from financial_statements;", null, function(err, result){
    if(err){
      console.log(err);
    }
    else{
      for(var i = 0; i < result.rowCount; i++){
        render_info.statements.push(
            {
              id: result.rows[i].statement_type_id,
              name: result.rows[i].statement_name,
              metrics: []
        })
      }
    }

    //Now query for all of the metrics
    db.query("SELECT * from metrics", null, function(err, result){
      if(err){
        console.log(err);
      }
      else{
        //loop through each row of the results and check with the statements already created
        for(var i = 0; i < result.rows.length; i++){
          for(var j = 0; j < render_info.statements.length; j++){
            if(result.rows[i].financial_statement_type == render_info.statements[j].id){
              render_info.statements[j].metrics.push(result.rows[i]);
            }
          }
        }
      }

      //Now get a list of companies that is available for plotting
      db.query('SELECT * from company', null, function(err, result){
        render_info.company = [];
        render_info.fama = [];
        render_info.sector = [];
        render_info.industry = [];

        for(var i = 0; i < result.rowCount; i++){
          //Build FAMA industry list
          if(render_info.fama.indexOf(result.rows[i].fama_industry) == -1){
            render_info.fama.push(result.rows[i].fama_industry);
          }
          //Build Sector list
          if(render_info.sector.indexOf(result.rows[i].sector) == -1){
            render_info.sector.push(result.rows[i].sector);
          }
          //Build industry list
          if(render_info.industry.indexOf(result.rows[i].industry) == -1){
            render_info.industry.push(result.rows[i].industry);
          }
          //Build company list
          render_info.company.push(result.rows[i]);
        }
        res.render('index', {render_info: render_info});
      });
    })
  });
});

router.all('/get_data', function(req, res, next){
  //ensure a body exists
  if(req.body && req.body.companies && req.body.metrics) {
    //query db for data
    var query_values = [];
    var query_string = "SELECT * FROM data WHERE (";
    var count = 1;

    for(var i = 0; i<req.body.companies.length; i++){
      query_string += "code = ";
      query_values.push(req.body.companies[i]);
      query_string += "$" + count++;
      query_string += "";
      if(i != req.body.companies.length -1){
        query_string += " OR "
      }
    }
    query_string += ") AND ( metric = ";
    query_string += "$" + count++;
    query_values.push(req.body.metrics.x);
    query_string += " OR metric = ";
    query_string += "$" + count++;
    query_values.push(req.body.metrics.y);
    query_string += " OR metric = ";
    query_string += "$" + count++;
    query_values.push(req.body.metrics.size);
    query_string += ");";

    db.query(query_string, query_values, function(err, result){
      if(err){
        console.log(err);
      }
      //Clustering algorithms switch.
      var companies = [];
      //Go through each row of the result
      for(var i = 0; i < result.rowCount; i++){
        //Set an index for the company name
        var index = -1;
        //Go through each company populated
        for(var j = 0; j < companies.length; j++){
          //IF The company exists in the reconstructing object, look for the date this row belongs to.
          if(companies[j].code == result.rows[i].code){
            index = j;
            var date_index = -1;
            //Loop through all the dates inside the company
            for(var k = 0; k < companies[j].data.length; k++){
              //check to see if there is a date match
              if(roundToNearestYear(result.rows[i].date) == companies[j].data[k].date){
                date_index = k;
                //check to see where this metric belongs
                if(result.rows[i].metric == req.body.metrics.x){
                  companies[j].data[k].metricX = result.rows[i].value;
                }
                //check which metric this row is storing
                if(result.rows[i].metric == req.body.metrics.y){
                  companies[j].data[k].metricY = result.rows[i].value;
                }
                //check which metric this row is storing
                if(result.rows[i].metric == req.body.metrics.size){
                  companies[j].data[k].metricSize = result.rows[i].value;
                }
                break;
              }
            }
            if(date_index == -1){
              //After this for loop if the date haven't been found, create new date
              var idate = roundToNearestYear(result.rows[i].date);

              companies[j].data.push({date: idate});
              //check to see where this metric belongs
              if(result.rows[i].metric == req.body.metrics.x){
                companies[j].data[k].metricX = result.rows[i].value;
              }
              //check which metric this row is storing
              if(result.rows[i].metric == req.body.metrics.y){
                companies[j].data[k].metricY = result.rows[i].value;
              }
              //check which metric this row is storing
              if(result.rows[i].metric == req.body.metrics.size){
                companies[j].data[k].metricSize = result.rows[i].value;
              }
            }
            break;
          }
        }
        if(index == -1){
          //company was not found in the reconstructing object
          var idate = roundToNearestYear(result.rows[i].date);
          var new_company = {
            code: result.rows[i].code,
            data: [{
              date: idate
            }]
          };
          //check which metric this row is storing
          if(result.rows[i].metric == req.body.metrics.x){
            new_company.data[0].metricX = result.rows[i].value;
          }
          //check which metric this row is storing
          if(result.rows[i].metric == req.body.metrics.y){
            new_company.data[0].metricY = result.rows[i].value;
          }
          //check which metric this row is storing
          if(result.rows[i].metric == req.body.metrics.size){
            new_company.data[0].metricSize = result.rows[i].value;
          }
          companies.push(new_company);
        }
      }

      //clusters is a list object where each child is a cluster and has an attribute name and companies.
      //further processing occurs on client side to filter for the dates to plot on graph.
      var clusters = [{
        name: 'cluster 1',
        companies: companies
      }];
      res.send(companies);
    })
  }
  else{
    res.send("Error: incorrect request received");
  }
});

router.all('/km_cluster', function(req, res, next){

  var data = req.body;

  //Select a point as c1 centrinoid
  var c1 = {
    x: null,
    y: null
  };
  c1.x = data[0].data[0].metricX;
  c1.y = data[0].data[0].metricY;

  //Find the point furtherest away from c1
  var max_dist = 0;
  var c2 = {};;
  //For every company
  for(var i = 0; i < data.length; i++){
    //For every date
    for(var j=0; j < data[i].data.length; j++) {
      if (data[i].data[j].date == 2015) {
        var td = {x: null, y: null}; //temp data
        td.x = data[i].data[j].metricX;
        td.y = data[i].data[j].metricY;
        var distance = get_sqrdistance(c1, td);
        if(distance > max_dist){
          max_dist = distance;
          c2.x = td.x;
          c2.y = td.y;
        }
      }
    }
  }

  console.log(c2);

  //while not converged
  //allocate data to the clusters
  //recalculate c2

  res.send(JSON.stringify(data));

});

function get_sqrdistance(a, b){
  var x_diff = b.x - a.x;
  var y_diff = b.y - a.y;
  return x_diff*x_diff + y_diff*y_diff;
}

function roundToNearestYear(sdate){
  var mdate = moment(sdate);
  var idate;
  if(mdate.month() > 5){
    idate = mdate.year();
  }
  else{
    idate = mdate.year() - 1;
  }
  return idate;
}

//Route to load data into the database.
//router.get('/load', function(req, res, next) {
//  dbloader.create_db();
//  res.render('load');
//});

module.exports = router;
